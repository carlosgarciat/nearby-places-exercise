//
//  ConnectionManager.swift
//  NearbyPlacesExercise
//
//  Created by Carlos García Torres on 08/03/2020.
//  Copyright © 2020 Carlos García Torres. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class ConnectionManager:NSObject {
    
    //Types of functions to get data
    enum FunctionType:String {
        
        case GETPLACES
        //Can be extended to more functions
    }
    
    
    //VARIABLES
    let baseURL:String = "https://maps.googleapis.com/maps/api/place/"
    weak var delegate:ConnectionManagerDelegate?
    
    override init() {
        
        super.init()
    }
    
    

    func getNearbyPlaces(lat:Double, long:Double, radius:Int, placeType:Constants.placeType) {
        
        let lastPartURL:String = "nearbysearch/json?location=\(lat),\(long)&radius=\(radius)&type=\(placeType.rawValue)&key=\(Constants.googleApiKey)"
        getData(lastPartURL: lastPartURL, functionType: .GETPLACES)
    }
    
    
    //Process the get with the correct url
    private func getData(lastPartURL:String, functionType:FunctionType) {
        
        let url = baseURL + lastPartURL
        print("url: \(url)")
        
        //We use Alamofire + SwiftJSON (quick and simple)
        Alamofire.request(url).responseJSON { (response) in
            switch response.result {
                
            case .success(let value):
                let json = JSON(value) //DEBUG
                
                print(json)
                self.processResponse(functionType: functionType, data:json)
                
            case .failure(let error):
                print(error)
                self.processErrorResponse(functionType: functionType, result: error.localizedDescription)
            }
        }
    }
    
    
    //Process the response and give the data to the appropiate delegate
    private func processResponse(functionType:FunctionType, data:JSON) {
        
        switch functionType {
            
        case .GETPLACES:
            
            if let status = data["status"].string {
                
                if (status == "OK") {
                    
                    if let placesArray = data["results"].array {
                        
                        self.delegate?.didGetNearbyPlacesOk(placesJSON: placesArray)
                        
                    } else {
                        
                        self.delegate?.didGetNearbyPlacesError(result: "JSON ERROR. No results")
                    }
                    
                } else {
                    self.delegate?.didGetNearbyPlacesError(result: "JSON ERROR. status:\(status)")
                }
                
                
            } else {
                //If it don't have a status, we have a problem in our query
                self.delegate?.didGetNearbyPlacesError(result: "JSON ERROR. No status")
            }
            
         //Can be extended
        }
    }
    
    
    
    //Gives the error response appropiate delegate
    private func processErrorResponse(functionType:FunctionType, result:String) {
        
        switch functionType {
         
        case .GETPLACES:
            self.delegate?.didGetNearbyPlacesError(result:result)
            break
            
        //Can be extended
        }
    }
}




protocol ConnectionManagerDelegate: class {
    
    func didGetNearbyPlacesOk(placesJSON:[JSON])
    func didGetNearbyPlacesError(result:String)
}
