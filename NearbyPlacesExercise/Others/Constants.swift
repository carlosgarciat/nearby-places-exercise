//
//  Constants.swift
//  NearbyPlacesExercise
//
//  Created by Carlos García Torres on 08/03/2020.
//  Copyright © 2020 Carlos García Torres. All rights reserved.
//

import Foundation

struct Constants {
    
    static let googleApiKey:String = "YOUR_API_KEY"  //You need to put your own key in order to run the app

    enum placeType:String {
        case BAR = "bar"
        case RESTAURANT = "restaurant"
        case CAFE = "cafe"
    }
    
    
    enum sortingType {
        case rating
        case name
        case open
        case distance
    }
    
    enum openNowType:Int {
        case OPEN = 0
        case UNKNOWN = 1
        case CLOSED = 2
    }
}
