//
//  DetailPresenter.swift
//  NearbyPlacesExercise
//
//  Created by Carlos García Torres on 14/03/2020.
//  Copyright © 2020 Carlos García Torres. All rights reserved.
//

import Foundation
import GooglePlaces

class DetailPresenter:NSObject {

    //MVP
    weak var view: DetailPresenterView?

    //VARIABLES
    var placesClient: GMSPlacesClient!

    
    init(view: DetailPresenterView) {
        
        super.init()
        
        self.view = view
        placesClient = GMSPlacesClient.shared()
    }
    
    
    func getLabelData(place:Place) {
        
        let ratingString:String = String(format: "%.1f", place.rating)
        let distanceString:String = String(format: "%.0fm", place.distance)
        let coordString:String = "\(place.lat)º \(place.lng)º"
        
        var openString:String
        var openColor:UIColor
        
        if place.open_now == .OPEN {
            openString = "OPEN"
            openColor = .green
        } else if place.open_now == .CLOSED {
            openString = "CLOSED"
            openColor = .red
        } else {
            openString = "--"
            openColor = .white
        }
        
        
        self.view?.infoToShow(name: "  \(place.name)  ", //For indentation purpose
                              rating: ratingString,
                              distance: distanceString,
                              coordinates: coordString,
                              open: openString,
                              openLabelColor: openColor)
    }
    
    
    func getPhoto(place:Place) {
        
        // !!
        //Snippet obtained from google Places SDK for iOS
        // !!
        
        // Specify the place data types to return (in this case, just photos).
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.photos.rawValue))!

        placesClient?.fetchPlace(fromPlaceID: place.place_id,
                                 placeFields: fields,
                                 sessionToken: nil, callback: {
          (place: GMSPlace?, error: Error?) in
          if let error = error {
            print("An error occurred: \(error.localizedDescription)")
            self.view?.showError(message: "An error occurred: \(error.localizedDescription)")
            
            return
          }
          if let place = place {
            // Get the metadata for the first photo in the place photo metadata list.
            
            //Testing I have realize that this snippet does not take into account when there are no photos
            //of the place
            if (place.photos == nil) {
                self.view?.showError(message: "There are no photos of this place")
                return
            }
            let photoMetadata: GMSPlacePhotoMetadata = place.photos![0]

            // Call loadPlacePhoto to display the bitmap and attribution.
            self.placesClient?.loadPlacePhoto(photoMetadata, callback: { (photo, error) -> Void in
              if let error = error {
                // TODO: Handle the error.
                print("Error loading photo metadata: \(error.localizedDescription)")
                self.view?.showError(message: "Error loading photo metadata: \(error.localizedDescription)")
                return
              } else {
                // Display the first image and its attributions.
                self.view?.obtainedImage(image: photo!)
                //self.imageView?.image = photo;
                //self.lblText?.attributedText = photoMetadata.attributions;
              }
            })
          }
        })
    }
    
}





protocol DetailPresenterView: class {
    
    func obtainedImage(image:UIImage)
    func showError(message:String)
    func infoToShow(name:String, rating:String, distance:String, coordinates:String, open:String, openLabelColor:UIColor)
}
