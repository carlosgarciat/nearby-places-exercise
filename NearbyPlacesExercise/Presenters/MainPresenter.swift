//
//  MapPresenter.swift
//  NearbyPlacesExercise
//
//  Created by Carlos García Torres on 08/03/2020.
//  Copyright © 2020 Carlos García Torres. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON

class MainPresenter:NSObject, CLLocationManagerDelegate, ConnectionManagerDelegate {
    
    
    
    //MVP
    weak var view: MainPresenterView?
    
    
    //VARIABLES
    var locationManager:CLLocationManager?
    var connectionManager:ConnectionManager?
    var lastLocation:CLLocation?
    var lookingFor:Constants.placeType = .BAR //First will check bars, next restaurants and last, cafes.
    var placesArray:[Place]! //Source of data of MainController
    
    init(view: MainPresenterView) {
        
        super.init()
        
        self.view = view
        
        placesArray = [Place]()
        
        connectionManager = ConnectionManager()
        connectionManager?.delegate = self
        
        //No need to change settings of locationManager for this exercise
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        
        locationManager?.requestWhenInUseAuthorization()
    }
    

    func updateButtonPressed() {
        
        //New search, places are removed
        placesArray.removeAll()
        
        //We ask for the location first, when obtained, we get the nearby places
        locationManager?.requestLocation() //We only need one location.
        
    }
    
    //Calculate distances to the user of the places array
    func updateDistances() {
        
        if (lastLocation != nil) {
            for place in placesArray {
            
                let placeLocation = CLLocation(latitude: place.lat, longitude: place.lng)
                place.distance = lastLocation?.distance(from: placeLocation) ?? 0
            }
            
        } else {
            //It will never show. Just for code coverage.
            view?.showError(message: "Bad user location. Distances cannot be calculated.")
        }
    }
    
    
    func getPlaces() {
        
        self.connectionManager?.getNearbyPlaces(lat: lastLocation!.coordinate.latitude,
                                                long: lastLocation!.coordinate.longitude,
                                                radius: 400, //Can be configurable
                                                placeType: lookingFor) //We ask first for bars
    }
    
    
    func orderPlacesBy(sortingType:Constants.sortingType) {
        
        switch sortingType {
            case .rating:
                placesArray.sort(by: { $0.rating > $1.rating })
            case .distance:
                placesArray.sort(by: { $0.distance < $1.distance })
            case .name:
                placesArray.sort(by: { $0.name < $1.name })
            
            case .open:
                placesArray.sort(by: { $0.open_now.rawValue < $1.open_now.rawValue }) //First open places
        }
        
        self.view?.orderedPlaces(places: placesArray)
    }
    
    
    //Process the JSON obtained from google places
    func processPlacesResult(placesJSON:[JSON]) {
        
        for result in placesJSON {
            
            let place = processPlaceResult(result: result)
            
            //We are processing 3 searchs that can deliver the same results, so we
            //filter them in a very simple way (can be improved)
            if (place != nil) &&
                (!isPlaceAlreadyInArray(name: place!.name)) {
                placesArray.append(place!)
            }
        }
    }
    
    
    func processPlaceResult(result:JSON) -> Place? {
        
        guard
            let place_id = result.dictionaryValue["place_id"]?.stringValue,
            let name = result.dictionaryValue["name"]?.stringValue,
            let lat = result["geometry"]["location"]["lat"].double,
            let lng = result["geometry"]["location"]["lng"].double
        
            else {
                print("This result is not ok. Proceeding with next")
                return nil
        }
        
        var rating:Double = 0
        //rating is optional
        if let rat = result.dictionaryValue["rating"]?.doubleValue {
            rating = rat
        } else {
            print("\(name) does not have rating")
        }
        
        var open_now:Constants.openNowType = .UNKNOWN
        //open_now is optional
        if let open = result["opening_hours"]["open_now"].bool {
            if (open) {
                open_now = .OPEN
            } else {
                open_now = .CLOSED
            }
        } else {
            print("\(name) does not have open_now")
        }
        
        //Everything went fine
        let place:Place = Place(place_id: place_id,
                                name: name,
                                rating: rating,
                                open_now: open_now,
                                lat: lat,
                                lng: lng)
        
        return place
    }
    
    
    func isPlaceAlreadyInArray(name:String) -> Bool {
        
        for place in placesArray {
            if place.name == name {
                return true
            }
        }
        return false
    }
    
    
    
    // --------
    //DELEGATES
    // --------
    
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(#function)
        
        if (locations.count > 0) &&
            (CLLocationCoordinate2DIsValid(locations.last!.coordinate)) {

            lastLocation = locations.last
            
            getPlaces()
            
        } else {
            view?.showError(message: "User location can't be obtained.\nRetry later")
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(#function)
        
        view?.showError(message: "User location can't be obtained.\nCheck location permission/retry later")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //print("\(#function)")
        
        
        if status == .notDetermined {
            print("Location permission: NOT DETERMINED")
            locationManager!.requestWhenInUseAuthorization()
            
        } else if status == .denied {
            
            print("Location permission: DENEGADOS.")
            view?.showError(message: "Location permission is denied. Please allow it to use the app.")
            
        } else if status == .authorizedAlways {
            
            print("Location permission: ALWAYS") //This won't happen. Only asking for when in use in plist
            
        } else if status == .authorizedWhenInUse {
            
            print("Location permission: WHEN IN USE")
       
        } else if status == .restricted {
            
            print("Location permission: RESTRICTED.")
            view?.showError(message: "Location permission is restricted. Please allow it to use the app.")

        }
    }
    
    
    
    
    
    //ConnectionManagerDelegate
    func didGetNearbyPlacesOk(placesJSON: [JSON]) {
        
        //We load
        processPlacesResult(placesJSON: placesJSON)
        
        if (lookingFor == .BAR) {
            
            lookingFor = .RESTAURANT
            getPlaces()
            
        } else if (lookingFor == .RESTAURANT) {
            
            lookingFor = .CAFE
            getPlaces()
            
        } else if (lookingFor == .CAFE) {
            
            //We have all the places. We update distances
            self.updateDistances()
            lookingFor = .BAR
            
            view?.updatePlaces(places: placesArray)
        }
    }
    
    func didGetNearbyPlacesError(result: String) {
        
        view?.showError(message: "Cannot obtain \(lookingFor.rawValue) places.\nError: \(result)\n\nPlease retry later")
    }
}


protocol MainPresenterView: class {
    
    func updatePlaces(places:[Place])
    func showError(message:String)
    func orderedPlaces(places:[Place])
}
