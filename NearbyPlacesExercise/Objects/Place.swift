//
//  Place.swift
//  NearbyPlacesExercise
//
//  Created by Carlos García Torres on 08/03/2020.
//  Copyright © 2020 Carlos García Torres. All rights reserved.
//

import Foundation
import SwiftyJSON

class Place:NSObject {
    
    var place_id:String
    var name:String
    var rating:Double
    var open_now:Constants.openNowType
    var lat:Double
    var lng:Double
    var distance:Double = 0 //Initialized to 0, updated later
    
    init(place_id:String, name:String, rating:Double, open_now:Constants.openNowType, lat:Double, lng:Double) {
        
        self.place_id = place_id
        self.name = name
        self.rating = rating
        self.open_now = open_now
        self.lat = lat
        self.lng = lng
    }
}
