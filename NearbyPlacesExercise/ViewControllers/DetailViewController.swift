//
//  DetailViewController.swift
//  NearbyPlacesExercise
//
//  Created by Carlos García Torres on 14/03/2020.
//  Copyright © 2020 Carlos García Torres. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {


    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var userRatingLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var openNowLabel: UILabel!
    @IBOutlet weak var loadingActivityView: UIActivityIndicatorView!
    
    
    //MVP
    lazy var presenter = DetailPresenter(view: self)
    
    //VARIABLES
    var place:Place?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //Some UI improvement
        self.nameLabel.layer.cornerRadius = 10
        self.nameLabel.layer.borderWidth = 1
        self.nameLabel.layer.borderColor = UIColor.white.cgColor
        
        //Loading data
        if (place != nil) {
            
            loading(opt: true)
            presenter.getPhoto(place:place!)
            presenter.getLabelData(place:place!)
            
        } else {
            print("place not set")
        }
    }
    
    func loading(opt:Bool) {
        DispatchQueue.main.async {
            if (opt) {
                self.loadingActivityView.startAnimating()
                self.loadingActivityView.isHidden = false
            } else {
                self.loadingActivityView.stopAnimating()
                self.loadingActivityView.isHidden = true
            }
        }
    }
    
    
    
    
    
    //BUTTONS
    @IBAction func backPressed(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
}


extension DetailViewController: DetailPresenterView {
    
    func obtainedImage(image: UIImage) {
        
        DispatchQueue.main.async {
            self.imageView.image = image
        }
        
        loading(opt: false)
    }
    
    
    func showError(message:String) {
        
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "Error",
                                                    message: message,
                                                    preferredStyle: .alert)
                
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""),
                                                    style: .default, handler: {
                                                        action in
                                                        
                    alertController.dismiss(animated: true, completion: nil)
            }))
            
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        loading(opt: false)
    }
    
    
    func infoToShow(name:String, rating:String, distance:String, coordinates:String, open:String, openLabelColor:UIColor) {
        
        DispatchQueue.main.async {
            
            self.nameLabel.text = name
            self.userRatingLabel.text = rating
            self.distanceLabel.text = distance
            self.coordinatesLabel.text = coordinates
            self.openNowLabel.text = open
            self.openNowLabel.textColor = openLabelColor
        }
    }
}
