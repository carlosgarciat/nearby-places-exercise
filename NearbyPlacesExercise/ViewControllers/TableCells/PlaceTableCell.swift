//
//  PlaceTableCell.swift
//  NearbyPlacesExercise
//
//  Created by Carlos García Torres on 14/03/2020.
//  Copyright © 2020 Carlos García Torres. All rights reserved.
//

import UIKit

class PlaceTableCell: UITableViewCell {
    

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Some change to get UI a little more interesting
        self.view.layer.cornerRadius = 10
        self.view.layer.borderWidth = 1
        self.view.layer.borderColor = UIColor.white.cgColor
    }
}
