//
//  MainViewController.swift
//  NearbyPlacesExercise
//
//  Created by Carlos García Torres on 08/03/2020.
//  Copyright © 2020 Carlos García Torres. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var ratingSortingButton: UIButton!
    @IBOutlet weak var nameSortingButton: UIButton!
    @IBOutlet weak var openSortingButton: UIButton!
    @IBOutlet weak var distanceSortingButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingActivityView: UIActivityIndicatorView!
    @IBOutlet weak var updatePlacesButton: UIButton!
    
    
    //MVP
    lazy var presenter = MainPresenter(view: self)
    
    //VARIABLES
    var orderBy:Constants.sortingType = .rating
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        doSomeUIImprovement()
        
        allowUserInteraction(opt: true)
        checkSorting()
    }
    
    
    func doSomeUIImprovement() {
        
        self.ratingSortingButton.layer.cornerRadius = 10
        self.ratingSortingButton.layer.borderWidth = 1
        self.ratingSortingButton.layer.borderColor = UIColor.white.cgColor
        
        self.nameSortingButton.layer.cornerRadius = 10
        self.nameSortingButton.layer.borderWidth = 1
        self.nameSortingButton.layer.borderColor = UIColor.white.cgColor
        
        self.openSortingButton.layer.cornerRadius = 10
        self.openSortingButton.layer.borderWidth = 1
        self.openSortingButton.layer.borderColor = UIColor.white.cgColor
        
        self.distanceSortingButton.layer.cornerRadius = 10
        self.distanceSortingButton.layer.borderWidth = 1
        self.distanceSortingButton.layer.borderColor = UIColor.white.cgColor
    }

    
    func checkSorting() {
        
        DispatchQueue.main.async {
            switch self.orderBy {
            case .rating:
                self.ratingSortingButton.backgroundColor = .yellow
                self.nameSortingButton.backgroundColor = .clear
                self.openSortingButton.backgroundColor = .clear
                self.distanceSortingButton.backgroundColor = .clear
            case .name:
                self.ratingSortingButton.backgroundColor = .clear
                self.nameSortingButton.backgroundColor = .yellow
                self.openSortingButton.backgroundColor = .clear
                self.distanceSortingButton.backgroundColor = .clear
            case .open:
                self.ratingSortingButton.backgroundColor = .clear
                self.nameSortingButton.backgroundColor = .clear
                self.openSortingButton.backgroundColor = .yellow
                self.distanceSortingButton.backgroundColor = .clear
            case .distance:
                self.ratingSortingButton.backgroundColor = .clear
                self.nameSortingButton.backgroundColor = .clear
                self.openSortingButton.backgroundColor = .clear
                self.distanceSortingButton.backgroundColor = .yellow
            }
        }
        
    }
    
    //To coordinate the loading process with the UI
    func allowUserInteraction(opt:Bool) {
        
        DispatchQueue.main.async {
            
            if opt {
                self.loadingActivityView.stopAnimating()
                self.loadingActivityView.isHidden = true
                self.nameSortingButton.isEnabled = true
                self.openSortingButton.isEnabled = true
                self.distanceSortingButton.isEnabled = true
                self.updatePlacesButton.isEnabled = true
            
            } else {
                self.loadingActivityView.startAnimating()
                self.loadingActivityView.isHidden = false
                self.nameSortingButton.isEnabled = false
                self.openSortingButton.isEnabled = false
                self.distanceSortingButton.isEnabled = false
                self.updatePlacesButton.isEnabled = false
            }
        }
    }
    
    // --------
    //DELEGATES
    // --------
    
    //UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.placesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeTableCell", for: indexPath) as! PlaceTableCell
        cell.nameLabel.text = presenter.placesArray[indexPath.row].name
        
        if (presenter.placesArray[indexPath.row].open_now == .OPEN) {
            cell.openLabel.text = "OPEN NOW"
            cell.openLabel.textColor = .green
        } else if (presenter.placesArray[indexPath.row].open_now == .CLOSED) {
            cell.openLabel.text = "CLOSED NOW"
            cell.openLabel.textColor = .red
        } else {
            cell.openLabel.text = "--"
            cell.openLabel.textColor = .white
        }
        
        cell.distanceLabel.text = String(format: "%.0fm", presenter.placesArray[indexPath.row].distance)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row:\(indexPath.row)")
        
        let detailViewController: DetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailViewController.place = presenter.placesArray[indexPath.row]
        
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    
    
    
    //BUTTONS ACTIONS
    @IBAction func updatePlacesPressed(_ sender: Any) {
        
        self.allowUserInteraction(opt: false)
        presenter.updateButtonPressed()
    }
    
    @IBAction func sortByRatingPressed(_ sender: Any) {
        
        self.allowUserInteraction(opt: false)
        orderBy = .rating
        checkSorting()
        presenter.orderPlacesBy(sortingType: .rating)
    }
    
    @IBAction func sortByNamePressed(_ sender: Any) {
        
        self.allowUserInteraction(opt: false)
        orderBy = .name
        checkSorting()
        presenter.orderPlacesBy(sortingType: .name)
    }
    
    @IBAction func sortByOpenPressed(_ sender: Any) {
        
        self.allowUserInteraction(opt: false)
        orderBy = .open
        checkSorting()
        presenter.orderPlacesBy(sortingType: .open)
    }
    
    @IBAction func sortByDistancePressed(_ sender: Any) {
        
        self.allowUserInteraction(opt: false)
        orderBy = .distance
        checkSorting()
        presenter.orderPlacesBy(sortingType: .distance)
    }
    
    
    
}


extension MainViewController: MainPresenterView {
    
    func updatePlaces(places: [Place]) {
        print(#function)
        
        //Data is loaded, we order the data as it should be
        self.presenter.orderPlacesBy(sortingType: orderBy)
    }
    
    func showError(message: String) {
        print(#function)
        
        checkSorting()
        
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "Error",
                                                    message: message,
                                                    preferredStyle: .alert)
                
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""),
                                                    style: .default, handler: {
                                                        action in
                                                        
                    alertController.dismiss(animated: true, completion: nil)
            }))
            
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        allowUserInteraction(opt: true)
    }
    
    func orderedPlaces(places: [Place]) {
        print(#function)
        
        //Data is loaded, we can reload table
        self.tableView.reloadData()
        
        self.allowUserInteraction(opt: true)
    }
}
